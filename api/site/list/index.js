
	var fs = require('fs');
	var path = require('path');
	var _ = require('lodash');
	var AdminDataFields = {
		Config: 'config',
		Dic: 'data/dic',
		Nav: 'data/nav'
	};

	module.exports = function(req, res, next) {

		var RootPath = res.locals.RootPath;
		var SitesPath = path.join(RootPath, 'site');
		var Data = [];

		fs.readdir(SitesPath, function(Error, Files) {

			if(Error) {

				return res.json({
					Path: req.path,
					Status: 'KO',
					Data: [Error]
				});
			}

			var SitePaths = _.filter(Files, function(File) {

				var FileStat = fs.statSync(path.join(SitesPath, File));
				var IsDir = FileStat.isDirectory();

				return File != 'common' && IsDir;
			});

			_.forEach(SitePaths, function(SitePath) {

				var DataPath = path.join(SitesPath, SitePath);
				var Site = {};

				_.forEach(AdminDataFields, function(DataField, Key) {

					Site[Key] = require(path.join(DataPath, DataField));
				})

				Site.Id = Site.Config.Id;
				Site.Name = Site.Config.Name;

				Data.push(Site);
			});

			res.json({
				Path: req.path,
				Status: 'Ok',
				Data: Data
			});
		});
	}