
	var fs = require('fs');
	var path = require('path');
	var _ = require('lodash');

	module.exports = function(req, res, next) {

		var Fields = res.locals.Form.Fields;
		var SiteId = Fields.site;
		var Data = JSON.stringify(Fields, null, 4);
		var FileName = SiteId + '_' + new Date().getTime() + '.json';
		var FilePath = path.join(__dirname, 'contacts', FileName);

		fs.writeFile(FilePath, Data, function(Error) {

			if(Error) {

				return res.json({
					Path: req.path,
					Status: 'ko',
					Message: Error
				});
			}
		});

		return res.json({
			Path: req.path,
			Status: 'OK',
			Message: 'Contacto guardado'
		});
	}