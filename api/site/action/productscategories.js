

	var fs = require('fs');
	var path = require('path');
	var _ = require('lodash');
	var WCApi = require('woocommerce-api');

	var ErrorCodes = {
		'400': 'Bad Request	Invalid request, e.g. using an unsupported HTTP method',
		'401': 'Unauthorized	Authentication or permission error, e.g. incorrect API keys',
		'404': 'Not Found	Requests to resources that don’t exist or are missing',
		'500': 'Internal Server Error'
	}

	module.exports = function(req, res, next) {

		var InAPI = typeof req == 'object';

		var SiteConfig = InAPI ? req.session.Config : global.SiteConfig;
		var SiteDataProductCategoriesFile = InAPI ?
											path.join(req.session.SitePath, 'data', 'productcategories')
											:
											global.SiteDataProductCategoriesFile;
		var Langs = SiteConfig.Lang;
		var Errors = [];
		var LangsLoaded = 0;

		function langLoaded(Lang, Error) {

			if(Error) {

				Errors.push(Error);
			}

			LangsLoaded++;

			if(LangsLoaded == _.size(Langs)) {

				if(Errors.length > 0) {

					return InAPI ? res.json({
									Path: req.path,
									Status: 'KO',
									Message: 'API. Error calling WC API: ' + Errors.join('|')
								})
								:
								console.log(Errors.join('|'));
				}

				return InAPI ? res.json({
								Path: req.path,
								Status: 'OK',
								Message: 'Products categories created'
							})
							:
							console.log('Products categories created');
			}
		}

		_.forEach(Langs, function(Language, Lang) {

			var WC = new WCApi({
				url: SiteConfig.WPSource + '/' + Lang,
				wpAPI: true,
				version: 'wc/v1',
				verifySsl: true,
				queryStringAuth: true,
				consumerKey: SiteConfig.WCClientKey,
				consumerSecret: SiteConfig.WCSecretKey
			});

			WC.get('products/categories', function(Error, Result) {

				if(Error) {

					return langLoaded(Lang, Error);
				}

				var JSResult = JSON.parse(Result.body);

				if(JSResult.data) {

					var Status = JSResult.data.status + '';
					return langLoaded(Lang, ErrorCodes[Status] + ' | ' + JSResult.message);
				}

				var Categories = _.map(JSResult, function(Categorie) {

					return {
						id: Categorie.id,
						name: Categorie.name,
						slug: Categorie.slug,
						parent: Categorie.parent
					}
				});

				var JSONData = JSON.stringify(Categories, null, 4);
				var SiteDataProductCategoriesFileLang = SiteDataProductCategoriesFile + Lang + '.json';

				fs.writeFile(SiteDataProductCategoriesFileLang, JSONData, function(Error) {

					if(Error) {

						return langLoaded(Lang, Error);
					}

					return langLoaded(Lang);
				});
			});
		});
	}