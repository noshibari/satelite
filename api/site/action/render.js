

	var fs = require('fs');
	var path = require('path');
	var _ = require('lodash');
	var mkdirp = require('mkdirp');
	var gulp = require('gulp');
	var changed = require('gulp-changed');

	_.templateSettings.interpolate = /<%=([\s\S]+?)%>/g;

	module.exports = function(req, res, next) {

		var InAPI = typeof req == 'object';

		var SiteConfig = InAPI ? req.session.Config : global.SiteConfig;
		var SiteCommonTemplatesDir = InAPI ?
									path.join(req.session.RootPath, 'theme', 'common')
									:
									global.SiteCommonTemplatesDir;
		var SiteThemeTemplatesDir = InAPI ?
									path.join(req.session.RootPath, 'theme', SiteConfig.Theme)
									:
									global.SiteThemeTemplatesDir;
		var SiteWebDir = InAPI ?
							path.join(req.session.RootPath, '..', 'web', SiteConfig.Id, 'www')
							:
							global.SiteWebDir;
		var SiteDataDir = InAPI ?
							path.join(req.session.RootPath, 'site', SiteConfig.Id, 'data')
							:
							global.SiteDataDir;

		var SRCData = {
			Config: SiteConfig,
			LangDefault: SiteConfig.LangDefault
		};
		var LangsRendered = 0;
		var LangsCount= _.size(SiteConfig.Lang);

		function langCreated(Lang, Errors) {

			LangsRendered++;

			if(LangsCount == LangsRendered) {

				if(Errors) {

					return InAPI ? res.json({
									Path: req.path,
									Status: 'KO',
									Message: 'API. Error in render: ' + Errors.join('|')
								})
								:
								console.log(Errors.join('|'));
				}

				return InAPI ? res.json({
								Path: req.path,
								Status: 'OK',
								Message: 'Render done'
							})
							:
							console.log('Render done');
			}
		}

		fs.readdir(SiteDataDir, function(Error, Files) {

			if(Error) {

				if(InAPI) {

					return res.json({
						Path: req.path,
						Status: 'KO',
						Message: 'API. Error reading site data: ' +
								  Error
					});
				}

				return console.log(Error);
			}

			_.forEach(Files, function(File) {

				var DataName = File.replace('.json', '');
				var DataFile = path.join(SiteDataDir, File);
				var JSONData = fs.readFileSync(DataFile);
				SRCData[DataName] = JSON.parse(JSONData);
			});

			/* Debug

			var JSONSRCData = JSON.stringify(SRCData, null, 4);
			var SRCDataFile = path.join(__dirname, 'srcdata.json');
			fs.writeFile(SRCDataFile, JSONSRCData, function(Error) {

				if(Error) { console.log(Error); }
			});

			*/

			// Utils

			global.ThumbPrefix = '-300x300.jpg';
			global.ViewPrefix = '-600x600.jpg';

			global.T = function(Lang, Key, Datas) {

				var DicEntry = SRCData.dic[Key];
				return DicEntry ? DicEntry[Lang] : Key;
			}

			global.addTemplate = function(TemplateSRC, Data) {

				var TemplateFile = path.join(SiteThemeTemplatesDir, TemplateSRC + '.shtml'); // Fragments always .shtml
				var TemplateHTML = fs.readFileSync(TemplateFile) + '';
				var Template = _.template(TemplateHTML);

				return Template({ Config: SiteConfig, Data: Data });
			}

			// Templates list

			var SiteTemplates = [];

			_.forEach(SiteConfig.CommonTemplates, function(Tmpl) {

				Tmpl.File = path.join(SiteCommonTemplatesDir, Tmpl.SRC + '.html');
				SiteTemplates.push(Tmpl);
			});

			_.forEach(SiteConfig.Templates, function(Tmpl) {

				Tmpl.File = path.join(SiteThemeTemplatesDir, Tmpl.SRC + '.html');
				SiteTemplates.push(Tmpl);
			});

			_.forEach(SiteConfig.Lang, function(Language, Lang) {

				// Process templates

				var TemplatesCount = SiteConfig.Templates.length;
				var TemplatesError = [];

				function templateComplete(TemplateSRC, Error) {

					if(Error) {

						TemplatesError.push(Error);
					}

					TemplatesCount--;
					if(TemplatesCount == 0) {

						return langCreated(Lang, TemplatesError);
					}
				}

				_.forEach(SiteTemplates, function(Template) {

					var TemplateFile = Template.File;
					var Serie = Template.Serie;
					var Dest = Template.DST ? Template.DST : '';
					var LangDestDir = Lang == SRCData.LangDefault ? '' : Lang;
					var DestDir = path.join(SiteWebDir, LangDestDir, Dest);
					var Name = Template.Name || Template.SRC;
					var SSILang = Lang == SRCData.LangDefault ? '' : Lang + '/';

					mkdirp(DestDir, function(Error) {

						if(Error) {

							return langCreated(Lang, Error);
						}

						if(Template.NoProcess) {

							var DestFile = path.join(DestDir, Name + '.html');
							fs.createReadStream(TemplateFile)
							  .pipe(fs.createWriteStream(DestFile));

							return templateComplete(Template.SRC);
						}

						fs.readFile(TemplateFile, function(Error, HTML) {

							var RenderTemplate;

							if(Error) {

								return templateComplete(Template.SRC, Error);
							}

							try {

								RenderTemplate = _.template(HTML);
							} catch(E){

								return templateComplete(Template.SRC, E);
							}

							if(Serie) {

								var SerieData = SRCData[Serie + Lang];
								var SerieCount = SerieData.length;

								_.forEach(SerieData, function(Data) {

									var Id = Data.id;
									var DestFile = path.join(DestDir, Name + '_' + Id + '.html');

									fs.writeFile(DestFile, RenderTemplate({
										Config: SiteConfig,
										SSILang: SSILang,
										Lang: Lang,
										Serie: Data
									}), function(Error) {

										if(Error) {

											return templateComplete(Template.SRC, Error);
										}

										SerieCount--;

										if(SerieCount == 0) {

											return templateComplete(Template.SRC);
										}
									});
								});
							} else {

								var DestFile = path.join(DestDir, Name + '.html');

								fs.writeFile(DestFile, RenderTemplate({
									SSILang: SSILang,
									Lang: Lang,
									SRCData: SRCData
								}), function(Error) {

									if(Error) {

										return templateComplete(Template.SRC, Error);
									}

									return templateComplete(Template.SRC);
								});
							}
						});
					});
				});
			});
		});
	}