

	var fs = require('fs');
	var path = require('path');
	var _ = require('lodash');
	var WCApi = require('woocommerce-api');
	var ErrorCodes = {
		'400': 'Bad Request	Invalid request, e.g. using an unsupported HTTP method',
		'401': 'Unauthorized	Authentication or permission error, e.g. incorrect API keys',
		'404': 'Not Found	Requests to resources that don’t exist or are missing',
		'500': 'Internal Server Error'
	}
	var SplitCustomDataFields = [
		'_product_images_promo',
		'_product_images_gallery',
		'_product_flip_book_pages'
	];
	var SplitCustomDataFieldsSeparator = '|';

	function parseCustomData(CustomData) {

		var Datas = CustomData[0];
		delete Datas[''];

		_.forEach(Datas, function(Data, Key) {

			if(_.indexOf(SplitCustomDataFields, Key) != -1) {

				Datas[Key] = Data.split(SplitCustomDataFieldsSeparator);
			}
		});

		return Datas;
	}

	module.exports = function(req, res, next) {

		var InAPI = typeof req == 'object';

		var SiteConfig = InAPI ? req.session.Config : global.SiteConfig;
		var SiteDataProductsFile = InAPI ?
									path.join(req.session.SitePath, 'data', 'products')
									:
									global.SiteDataProductsFile;
		var Langs = SiteConfig.Lang;
		var Errors = [];
		var LangsLoaded = 0;

		function langLoaded(Lang, Error) {

			if(Error) {

				Errors.push(Error);
			}

			LangsLoaded++;

			if(LangsLoaded == _.size(Langs)) {

				if(Errors.length > 0) {

					return InAPI ? res.json({
									Path: req.path,
									Status: 'KO',
									Message: 'API. Error calling WC API: ' + Errors.join('|')
								})
								:
								console.log(Errors.join('|'));
				}

				return InAPI ? res.json({
								Path: req.path,
								Status: 'OK',
								Message: 'Products created'
							})
							:
							console.log('Products created');
			}
		}

		_.forEach(Langs, function(Language, Lang) {

			var WC = new WCApi({
				url: SiteConfig.WPSource + '/' + Lang,
				wpAPI: true,
				version: 'wc/v1',
				verifySsl: true,
				queryStringAuth: true,
				consumerKey: SiteConfig.WCClientKey,
				consumerSecret: SiteConfig.WCSecretKey
			});

			WC.get('products?filter[meta]=true', function(Error, Result) {

				if(Error) {

					return langLoaded(Lang, Error);
				}

				var JSResult = JSON.parse(Result.body);

				if(JSResult.data) {

					var Status = JSResult.data.status + '';

					return langLoaded(Lang, ErrorCodes[Status] + ' | ' + JSResult.message);
				}

				var Products = _.map(JSResult, function(Product) {

					return {
						id: Product.id,
						name: Product.name,
						slug: Product.slug,
						catalog_visibility: Product.catalog_visibility,
						featured: Product.featured,
						description: Product.description,
						short_description: Product.short_description,
						sku: Product.sku,
				        price: Product.price,
				        regular_price: Product.regular_price,
				        sale_price: Product.sale_price,
				        on_sale: Product.on_sale,
				        purchasable: Product.purchasable,
				        weight: Product.weight,
				        dimensions: Product.dimensions,
				        categorieslist: _.map(Product.categories, function(C) { return C.slug; }),
				        categories: Product.categories,
				        images: Product.images,
				        attributes: Product.attributes,
				        menu_order: Product.menu_order,
				        stock_quantity: Product.stock_quantity,
				        wc_productdata_options: parseCustomData(Product.wc_productdata_options)
					}
				});

				var JSONData = JSON.stringify(Products, null, 4);
				var SiteDataProductsFileLang = SiteDataProductsFile + Lang + '.json';

				fs.writeFile(SiteDataProductsFileLang, JSONData, function(Error) {

					if(Error) {

						return langLoaded(Lang, Error);
					}

					return langLoaded(Lang);
				});
			});
		});
	}