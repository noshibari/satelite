

	var fs = require('fs');
	var path = require('path');
	var _ = require('lodash');
	var WP =require('wpapi');

	module.exports = function(req, res, next) {

		var InAPI = typeof req == 'object';

		var SiteConfig = InAPI ? req.session.Config : global.SiteConfig;
		var SiteDataPagesFile = InAPI ?
								path.join(req.session.SitePath, 'data', 'pages.json')
								:
								global.SiteDataPagesFile;
		var Langs = SiteConfig.Lang;
		var Errors = [];
		var LangsLoaded = 0;
		function langLoaded(Lang, Error) {

			if(Error) {

				Errors.push(Error);
			}

			LangsLoaded++;

			if(LangsLoaded == _.size(Langs)) {

				if(Errors.length > 0) {

					return InAPI ? res.json({
									Path: req.path,
									Status: 'KO',
									Message: Errors.join('|')
								})
								:
								console.log(Errors.join('|'));
				}

				return InAPI ? res.json({
								Path: req.path,
								Status: 'OK',
								Message: 'Pages data created'
							})
							:
							console.log('Pages data created');
			}
		}

		_.forEach(Langs, function(Language, Lang) {

			var wp = new WP({ endpoint: SiteConfig.WPSource + '/' + Lang + '/wp-json' });

			wp
			.pages()
			.category(SiteConfig.Id)
			.perPage(99)
			.then(function(Pages) {

				var MapData = _.map(Pages, function(Page) {

					return {
						id: Page.id,
						slug: Page.slug,
						title: Page.title,
						content: Page.content,
						excerpt: Page.excerpt,
						featured_media: Page.featured_media,
						categories: Page.categories
					}
				})

				var JSONData = JSON.stringify(MapData, null, 4);
				var SiteDataPagesFileLang = SiteDataPagesFile + Lang + '.json';

				fs.writeFile(SiteDataPagesFileLang, JSONData, function(Error) {

					if(Error) {

						return langLoaded(Lang, 'Error writing pages data file: ' + Error);
					}

					return langLoaded(Lang);
				})
			})
			.catch(function(Error) {

				return langLoaded(Lang, 'Error getting wordpress pages: ' + Error);
			});
		});
	}