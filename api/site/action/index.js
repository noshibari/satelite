
	var fs = require('fs');
	var path = require('path');
	var _ = require('lodash');

	module.exports = function(req, res, next) {

		var SiteId = req.session.Form.Fields.Id;
		var Action = req.session.Form.Fields.Action;

		if(!SiteId || !Action) {

			return res.json({
				Path: req.path,
				Status: 'KO',
				Message: 'Site ID & Action required'
			});
		}

		var RootPath = req.session.RootPath;
		var SitesPath = path.join(RootPath, 'site');
		var SitePath = path.join(SitesPath, SiteId);

		fs.exists(SitePath, function(Exists) {

			if(!Exists) {

				return res.json({
					Path: req.path,
					Status: 'KO',
					Message: 'Site doesn\'t exists'
				});
			}

			try {

				var ConfigPath = path.join(SitePath, 'config');
				req.session.SitePath = SitePath;
				req.session.Config = require(ConfigPath);

				try {

					var Process = require('./' + Action);
					Process(req, res, next);

				} catch(Error) {

					console.log(Error);

					return res.json({
						Path: req.path,
						Status: 'KO',
						Message: 'Error in component ' + Action
					});
				}
			} catch(Error) {

				return res.json({
					Path: req.path,
					Status: 'KO',
					Message: 'Error in site config'
				});
			}
		});
	}