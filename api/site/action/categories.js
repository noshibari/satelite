

	var fs = require('fs');
	var path = require('path');
	var _ = require('lodash');
	var WP =require('wpapi');

	module.exports = function(req, res, next) {

		var InAPI = typeof req == 'object';

		var SiteConfig = InAPI ? req.session.Config : global.SiteConfig;
		var SiteDataCategoriesFile = InAPI ?
								path.join(req.session.SitePath, 'data', 'categories.json')
								:
								global.SiteDataCategoriesFile;
		var Langs = SiteConfig.Lang;
		var Errors = [];
		var LangsLoaded = 0;
		function langLoaded(Lang, Error) {

			if(Error) {

				Errors.push(Error);
			}

			LangsLoaded++;

			if(LangsLoaded == _.size(Langs)) {

				if(Errors.length > 0) {

					return InAPI ? res.json({
									Path: req.path,
									Status: 'KO',
									Message: Errors.join('|')
								})
								:
								console.log(Errors.join('|'));
				}

				return InAPI ? res.json({
								Path: req.path,
								Status: 'OK',
								Message: 'Pages data created'
							})
							:
							console.log('Pages data created');
			}
		}

		_.forEach(Langs, function(Language, Lang) {

			var wp = new WP({ endpoint: SiteConfig.WPSource + '/' + Lang + '/wp-json' });

			wp
			.categories()
			.perPage(99)
			.then(function(Categories) {

				var MapData = _.map(Categories, function(Categorie) {

					return {
						id: Categorie.id,
						slug: Categorie.slug,
						name: Categorie.name,
						parent: Categorie.parent
					}
				})

				var JSONData = JSON.stringify(MapData, null, 4);
				var SiteDataCategoriesFileLang = SiteDataCategoriesFile + Lang + '.json';

				fs.writeFile(SiteDataCategoriesFileLang, JSONData, function(Error) {

					if(Error) {

						return langLoaded(Lang, 'Error writing categories data file: ' + Error);
					}

					return langLoaded(Lang);
				})
			})
			.catch(function(Error) {

				return langLoaded(Lang, 'Error getting wordpress categories: ' + Error);
			});
		});
	}