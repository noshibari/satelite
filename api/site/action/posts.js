

	var fs = require('fs');
	var path = require('path');
	var _ = require('lodash');
	var WP =require('wpapi');

	module.exports = function(req, res, next) {

		var InAPI = typeof req == 'object';

		var SiteConfig = InAPI ? req.session.Config : global.SiteConfig;
		var SiteDataPostsFile = InAPI ?
								path.join(req.session.SitePath, 'data', 'posts.json')
								:
								global.SiteDataPostsFile;
		var Langs = SiteConfig.Lang;
		var Errors = [];
		var LangsLoaded = 0;
		function langLoaded(Lang, Error) {

			if(Error) {

				Errors.push(Error);
			}

			LangsLoaded++;

			if(LangsLoaded == _.size(Langs)) {

				if(Errors.length > 0) {

					return InAPI ? res.json({
									Path: req.path,
									Status: 'KO',
									Message: Errors.join('|')
								})
								:
								console.log(Errors.join('|'));
				}

				return InAPI ? res.json({
								Path: req.path,
								Status: 'OK',
								Message: 'Posts data created'
							})
							:
							console.log('Posts data created');
			}
		}

		_.forEach(Langs, function(Language, Lang) {

			var wp = new WP({ endpoint: SiteConfig.WPSource + '/' + Lang + '/wp-json' });

			wp
			.posts()
			.category(SiteConfig.Id)
			.perPage(99)
			.then(function(Posts) {

				var MapData = _.map(Posts, function(Post) {

					return {
						id: Post.id,
						slug: Post.slug,
						title: Post.title,
						content: Post.content,
						excerpt: Post.excerpt,
						featured_media: Post.featured_media,
						categories: Post.categories
					}
				})

				var JSONData = JSON.stringify(MapData, null, 4);
				var SiteDataPostsFileLang = SiteDataPostsFile + Lang + '.json';

				fs.writeFile(SiteDataPostsFileLang, JSONData, function(Error) {

					if(Error) {

						return langLoaded(Lang, 'Error writing posts data file: ' + Error);
					}

					return langLoaded(Lang);
				})
			})
			.catch(function(Error) {

				return langLoaded(Lang, 'Error getting wordpress posts: ' + Error);
			});
		});
	}