
	module.exports = function(req, res, next) {

		var Action = req.session.Form.Fields.Action;

		if(!Action) {

			return res.json({
				Path: req.path,
				Status: 'KO',
				Message: 'Action required'
			});
		}

		try {

			var Process = require('./' + Action);
			Process(req, res, next);

		} catch(Error) {

			console.log(Error);

			return res.json({
				Path: req.path,
				Status: 'KO',
				Message: 'Error in component ' + Action
			});
		}
	}