
	var APIKey = '07b0aaeaa7dc9626da5c0e538747d009d1c7cce9fa812870257d3123772dcbd0';
	var kickbox = require('kickbox').client(APIKey).kickbox();

	module.exports = function(req, res, next) {

		var Fields = req.session.Form.Fields;
		var EMail = Fields.EMail;

		if(!EMail) {

			return res.json({
				Path: req.path,
				Status: 'KO',
				Message: 'EMail required'
			});
		}

		kickbox.verify(EMail, function(Error, Response){

			if(Error) {

				return res.json({
					Path: req.path,
					Status: 'KO',
					Message: Error
				});
			}

			var Result = (Response.body.result);

			if(Result == 'deliverable') {

				return res.json({
					Path: req.path,
					Status: 'OK',
					Message: 'EMail ' + EMail + ' exists'
				});
			}

			return res.json({
				Path: req.path,
				Status: 'KO',
				Message: 'User or domain in "' + EMail + '" doesn\'t exists'
			});
		});
	}