
	module.exports = {
		Id: 'intimateshibari',
		Name: 'Intimate Shibari',
		WebSite: 'http://intimateshibari.com',
		WPSource: 'https://poeticsoft.com',
		WCClientKey: 'ck_3e4f627113460b58554a3a5557bc06950dc30eb8',
		WCSecretKey: 'cs_50e7a5063b9c3c5f601067fcb5faab15e720f65a',
		APIRoot: 'http://satelite.poeticsoft.com/api',
		Theme: 'timer',
		Image: {
			Size: {
				thumb: {
					Format: 'jpg',
					Size: 200
				},
				web: {
					Format: 'jpg',
					Size: 640
				},
				view: {
					Format: 'jpg'
				}
			}
		},
		Lang: {
			es: 'Castellano',
			en: 'English',
			ca: 'Català'
		},
		LangDefault: 'es',
		CommonTemplates: [
			{
				SRC: 'ssi_detectmobile',
				NoProcess: true
			}
		],
		Templates: [
			{
				SRC: 'index', // index2 para
				Name: 'index'
			},
			{ SRC: 'ssi_commonhead' },
			{ SRC: 'ssi_header' },
			{ SRC: 'ssi_hero' },
			{ SRC: 'ssi_about' },
			{ SRC: 'ssi_portfolio' },
			{ SRC: 'ssi_calltoaction' },
			{ SRC: 'ssi_footer' },
			{
				SRC: 'works/index',
				DST: 'works',
				Name: 'index',
			},
			{
				SRC: 'works/ssi_works_menu',
				DST: 'works',
				Name: 'ssi_works_menu'
			},
			{
				SRC: 'works/ssi_works',
				DST: 'works',
				Name: 'ssi_works'
			},
			{
				SRC: 'works/work',
				DST: 'works',
				Name: 'work',
				Serie: 'products'
			},
			{ SRC: 'contact' }
		],
		JS: [
			{
				Name: 'main',
				Files: [
					'theme/timer/js/vendor/modernizr-2.6.2.min.js',
					'site/common/js/jquery.min.js',
					'site/common/js/simplecart/simpleCart.js',
					'site/common/js/jqueryvalidation/dist/jquery.validate.min.js',
					'site/common/js/isslider.js',
					'theme/timer/js/owl.carousel.min.js',
					'theme/timer/js/bootstrap.min.js',
					'theme/timer/js/wow.min.js',
					'theme/timer/js/customslider.js',
					'theme/timer/js/jquery.fancybox.js',
					'theme/timer/js/underscore.deepxtend.min.js',
					'theme/timer/js/main.js',
					'site/common/js/app.js'
				]
			},
			{
				Name: 'flipbook',
				Files: [
					'site/common/js/flipbook/turn.js',
					'site/common/js/flipbook/main.js'
				]
			}
		]
	}