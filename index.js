
	var path = require('path');
	var express = require('express');
	var session = require('express-session');
	var APP = express();
	var Dir = require('node-dir');
	var Formidable = require('formidable');
	var Port = process.env.PORT || 5000;
	var Router = express.Router();
	var APIRoot = '/api';
	var ApiDir = path.join(__dirname, 'api');

	APP.use(session({
		secret: '9eb11152f90174e982ebacdbab1599ce203abe75',
		resave: false,
		saveUninitialized: true
	}));

	APP.use(express.static(__dirname));

	Router.use(function (req, res, next) {

  		res.setHeader('Access-Control-Allow-Origin', '*');
  		res.setHeader('Access-Control-Allow-Headers', 'Content-Type');

		if(req.method != 'POST') {

			return res.end(JSON.stringify({
				Path: req.path,
				Status: 'KO',
				Message: 'API. Error, only POST allowed'
			}));
		}

		var Form = new Formidable.IncomingForm();

		Form.parse(req, function(Error, Fields, Files) {

			if(Error) {

				return res.json({
					Path: req.path,
					Status: 'KO',
					Message: 'API. Error parsing form: ' + Error.message
				});
			}

			console.log(Fields);

			req.session.RootPath = path.resolve(__dirname);
			req.session.Form = {
				Fields: Fields,
				Files: Files
			};

			next();
		});
	});

	Router.all('/', require('./api'));

	console.log('');
	console.log('----------------------------------------------------------')
	console.log('ROUTES');

	Dir.subdirs(ApiDir, function(Error, Dirs) {

		if (Error) {

			return console.log(Error);
		}

		Dirs.forEach(function(Dir) {

			var CleanDir = Dir.replace(/\\/g, '/');
			var ApiIndex = CleanDir.indexOf('api') + 3;
			var RoutePath = CleanDir.substring(ApiIndex);

			try {

				console.log('----------------------------------------------------------');
				console.log(RoutePath);
				console.log(Dir);

				Router.all(RoutePath, require(Dir));

			} catch(Error) {

				console.log('Routing error: ' + Error.message);
			}
		});

		APP.use(APIRoot, Router);

		APP.listen(Port);

		console.log('----------------------------------------------------------');
		console.log('');
		console.log('----------------------------------------------------------');
		console.log('API Server listening at ' + Port);
		console.log('----------------------------------------------------------');
	});