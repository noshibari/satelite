
	define([
	],
	function(
	) {
		'use strict';

		return {
			SiteList: {
				transport: {
					read: {
						url: 'https://satelite.herokuapp.com/api/site/list'
					}
				}
			},
			SiteAction: {
				transport: {
					read: {
						url: 'https://satelite.herokuapp.com/api/site/action'
					}
				}
			}
		};
	});