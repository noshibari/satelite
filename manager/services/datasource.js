
	define([
		'underscore',
		'services/datasourceconfig'
	],
	function(
		_,
		Config
	) {
		'use strict';

		var DataSourceTransport = {
			url: 'http://satelite.poeticsoft.com/api',
			type: 'POST',
			dataType: 'json',
			contentType: 'application/json',
			processData: false
		}

		// Configuración comun de los datasources

		var CommonDataSourceConfig = {
			transport: {
				read: DataSourceTransport,
				update: DataSourceTransport,
				create: DataSourceTransport,
				destroy: DataSourceTransport,
				parameterMap: function(Data, Type) {

					return JSON.stringify(Data);
				}
			},
			schema: {
				data: 'Data',
				errors: function(Response) {

					if(Response.Status == 'KO') {

						return Response.Message;
					}

					return null;
				}
			}
		};

		var DataSources = {};

		return function() {

			return {

				getDataSource: function(Id) {

					if(!DataSources[Id]) {

						var DataSourceConfig = _.deepExtend({}, CommonDataSourceConfig, Config[Id]);
						DataSources[Id] = new kendo.data.DataSource(DataSourceConfig);
					}

					return DataSources[Id];
				}
			}
		};
	});