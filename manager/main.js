

	requirejs([
		'config'
	],function (
		Config
	) {

		'use strict';

		requirejs.config(Config)([ 'app' ]);
	});