
	define([
		'text!./sites.html',
		'text!./site.html',
		'less!./sites.less',
		'kendo.grid.min',
		'directive/messages/messages'
	], function(
		Template,
		SiteTemplate,
		Grid,
		Messages
	) {

		'use strict';

		var controller = [
			'DataSourceService',
			'$scope',
			'$rootScope',
			'$timeout',
		function controller(
			DataSourceService,
			$scope,
			$rootScope,
			$timeout
		) {

			var actualizar = function(E) {

	            E.preventDefault();

	            var $Target = $(E.target);

	            if($Target.attr('disabled')) {

	            	return false;
	            }

	            var $TR = $Target.closest('tr');
	            var $Buttons = $TR.find('.k-button');
	            var Data = this.dataItem($TR);
	            var Class = $Target.attr('class');
	            var ActionClass = _.find(Class.split(' '), function(ClassName) {

	            	return ClassName.indexOf('k-grid') == 0;
	            });
	            var Action = ActionClass.replace('k-grid-', '');
	            var DataSource = DataSourceService.getDataSource('SiteAction');

	            $Buttons.attr('disabled', true);

	            function reset() {

					DataSource.unbind('change', change);
					DataSource.unbind('error', change);
					$Buttons.removeAttr('disabled');
	            }

				function error(E) {

					$rootScope.$broadcast('Message', {
						Type: 'Error',
						Message: E.errors
					});

					reset();
				}

				function change(E) {

					$rootScope.$broadcast('Message', {
						Type: 'Info',
						Message: 'Action ' + Action + ' in site ' + Data.Name + ' complete.'
					});

					reset();
				}

	            DataSource.bind('change', change);
	            DataSource.bind('error', error);

				$rootScope.$broadcast('Message', {
					Type: 'Warning',
					Message: 'Action ' + Action + ' in site ' + Data.Name + ' started.'
				});

				$timeout(function() {

					DataSource.read({
		            	Id: Data.Id,
		            	Action: Action.toLowerCase()
		            });
		        }, 1000);

	            $Target.blur();

	            return false;
	        }

			var DataSource = DataSourceService.getDataSource('SiteList');

			$scope.GridOptions = {
				dataSource: DataSource,
				columns: [
					{
						field: "Name",
						title: "Site"
					},
					{
						command: [
							{
								name: 'ActualizarWEB',
								text: 'WEB',
								click: actualizar
							},
							{
								name: 'ActualizarWP',
								text: 'WP',
								click: actualizar
							},
							{
								name: 'ActualizarProducts',
								text: 'Productos',
								click: actualizar
							}
						],
						title: '&nbsp;',
						width: '255px'
					}
				],
				detailTemplate: SiteTemplate
			}
		}];

		return {
			controller: controller,
			template: Template
		};
	});
