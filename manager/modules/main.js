
	define([
		'angular',
		'angular-ui-router',
		'angular-sanitize',
		'kendo.angular.min'
	], function(
		A,
		ARouter,
		ASanitize
	) {

		'use strict';

		return angular.module('Main', [
			'ui.router',
			'ngSanitize',
    		'kendo.directives'
		]);
	});
