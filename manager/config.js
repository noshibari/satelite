
	define(
		[

		],
		function (
		) {

			'use strict';

			window.less = {
				relativeUrls: true,
				env: 'development',
				logLevel: 1,
				async: true
			};

			return {
				packages: [
					{
						name: 'less',
						location: 'libs/require-less',
						main: 'less'
					},
					{
						name: 'css',
						location: 'libs/require-css',
						main: 'css.min'
					}
				],
				config: {
					less: {
					    env: 'development',
					    logLevel: 3,
					    async: true,
					    fileAsync: true
					},
					text: {
						useXhr: function (url, protocol, hostname, port) { return true; }
					}
				},
				paths: {
					text: 'libs/text/text',
					underscore: 'libs/underscore/underscore.deepxtend.min',
					jquery: 'libs/kendo-ui/js/jquery.min',
					angular: 'libs/kendo-ui/js/angular.min',
					'kendo.angular': 'libs/kendo-ui/js/kendo.angular',
					'angular-sanitize': 'libs/angular-sanitize/angular-sanitize',
					'angular-ui-router': 'libs/angular-ui-router/release/angular-ui-router',
					'kendo.core.min': 'libs/kendo-ui/js/kendo.core.min',
					'kendo.angular.min': 'libs/kendo-ui/js/kendo.angular.min',
					'kendo.data.min': 'libs/kendo-ui/js/kendo.data.min',
					'kendo.grid.min': 'libs/kendo-ui/js/kendo.grid.min',
					'kendo.autocomplete.min': 'libs/kendo-ui/js/kendo.autocomplete.min',
					'kendo.listview.min': 'libs/kendo-ui/js/kendo.listview.min',
					'kendo.list.min': 'libs/kendo-ui/js/kendo.list.min',
					'kendo.data.odata.min': 'libs/kendo-ui/js/kendo.data.odata.min',
					'kendo.data.xml.min': 'libs/kendo-ui/js/kendo.data.xml.min',
					'kendo.columnsorter.min': 'libs/kendo-ui/js/kendo.columnsorter.min',
					'kendo.window.min': 'libs/kendo-ui/js/kendo.window.min',
					'kendo.editable.min': 'libs/kendo-ui/js/kendo.editable.min',
					'kendo.filtermenu.min': 'libs/kendo-ui/js/kendo.filtermenu.min',
					'kendo.columnmenu.min': 'libs/kendo-ui/js/kendo.columnmenu.min',
					'kendo.pager.min': 'libs/kendo-ui/js/kendo.pager.min',
					'kendo.selectable.min': 'libs/kendo-ui/js/kendo.selectable.min',
					'kendo.groupable.min': 'libs/kendo-ui/js/kendo.groupable.min',
					'kendo.sortable.min': 'libs/kendo-ui/js/kendo.sortable.min',
					'kendo.reorderable.min': 'libs/kendo-ui/js/kendo.reorderable.min',
					'kendo.resizable.min': 'libs/kendo-ui/js/kendo.resizable.min',
					'kendo.ooxml.min': 'libs/kendo-ui/js/kendo.ooxml.min',
					'kendo.excel.min': 'libs/kendo-ui/js/kendo.excel.min',
					'kendo.progressbar.min': 'libs/kendo-ui/js/kendo.progressbar.min',
					'kendo.pdf.min': 'libs/kendo-ui/js/kendo.pdf.min',
					'kendo.popup.min': 'libs/kendo-ui/js/kendo.popup.min',
					'kendo.draganddrop.min': 'libs/kendo-ui/js/kendo.draganddrop.min',
					'kendo.fx.min': 'libs/kendo-ui/js/kendo.fx.min',
					'kendo.datepicker.min': 'libs/kendo-ui/js/kendo.datepicker.min',
					'kendo.numerictextbox.min': 'libs/kendo-ui/js/kendo.numerictextbox.min',
					'kendo.validator.min': 'libs/kendo-ui/js/kendo.validator.min',
					'kendo.binder.min': 'libs/kendo-ui/js/kendo.binder.min',
					'kendo.dropdownlist.min': 'libs/kendo-ui/js/kendo.dropdownlist.min',
					'kendo.menu.min': 'libs/kendo-ui/js/kendo.menu.min',
					'kendo.userevents.min': 'libs/kendo-ui/js/kendo.userevents.min',
					'kendo.color.min': 'libs/kendo-ui/js/kendo.color.min',
					'kendo.calendar.min': 'libs/kendo-ui/js/kendo.calendar.min',
					'kendo.drawing.min': 'libs/kendo-ui/js/kendo.drawing.min',
					'kendo.view.min': 'libs/kendo-ui/js/kendo.view.min',
					'kendo.multiselect.min': 'libs/kendo-ui/js/kendo.multiselect.min',
					'kendo.dropdownlist.min': 'libs/kendo-ui/js/kendo.dropdownlist.min',
					'kendo.dom.min': 'libs/kendo-ui/js/kendo.dom.min',
					'kendo.treeview.draganddrop.min': 'libs/kendo-ui/js/kendo.treeview.draganddrop.min',
					'kendo.treelist.min': 'libs/kendo-ui/js/kendo.treelist.min',
					'kendo.combobox.min': 'libs/kendo-ui/js/kendo.combobox.min',
					'kendo.colorpicker.min': 'libs/kendo-ui/js/kendo.colorpicker.min',
					'kendo.imagebrowser.min': 'libs/kendo-ui/js/kendo.imagebrowser.min',
					'kendo.filebrowser.min': 'libs/kendo-ui/js/kendo.filebrowser.min',
					'kendo.slider.min': 'libs/kendo-ui/js/kendo.slider.min',
					'kendo.upload.min': 'libs/kendo-ui/js/kendo.upload.min',
					'kendo.editor.min': 'libs/kendo-ui/js/kendo.editor.min',
					'kendo.mobile.scroller.min': 'libs/kendo-ui/js/kendo.mobile.scroller.min',
					'kendo.mobile.actionsheet.min': 'libs/kendo-ui/js/kendo.mobile.actionsheet.min',
					'kendo.mobile.pane.min': 'libs/kendo-ui/js/kendo.mobile.pane.min',
					'kendo.mobile.shim.min': 'libs/kendo-ui/js/kendo.mobile.shim.min',
					'kendo.mobile.popover.min': 'libs/kendo-ui/js/kendo.mobile.popover.min',
					'kendo.mobile.loader.min': 'libs/kendo-ui/js/kendo.mobile.loader.min',
					'kendo.mobile.view.min': 'libs/kendo-ui/js/kendo.mobile.view.min',
					'kendostyles': 'libs/kendo-ui/src/styles'
				},
				shim: {
					'angular': {
  						'deps': [ 'jquery' ],
						'exports': 'angular'
					},
					'angular-sanitize': {
						'deps': [ 'angular' ]
					},
					'angular-ui-router': {
						'deps': [ 'angular' ]
					},
					'kendo.angular': {
						'deps': [
							'angular',
							'kendo.core.min'
						]
					},
				    'kendo.core.min': {
				      'deps': [ 'angular' ],
				      'exports': 'kendo'
				    },
				    'kendo.data.min': {
				      'deps': [ 'kendo.core.min' ]
				    },
				    'kendo.grid.min': {
				      'deps': [
				        'kendo.data.min',
				        'kendo.columnsorter.min',
				        'kendo.editable.min',
				        'kendo.window.min',
				        'kendo.filtermenu.min',
				        'kendo.columnmenu.min',
				        'kendo.groupable.min',
				        'kendo.pager.min',
				        'kendo.selectable.min',
				        'kendo.sortable.min',
				        'kendo.reorderable.min',
				        'kendo.resizable.min',
				        'kendo.mobile.actionsheet.min',
				        'kendo.mobile.pane.min',
				        'kendo.ooxml.min',
				        'kendo.progressbar.min',
				        'kendo.excel.min',
				        'kendo.pdf.min'
				      ]
				    },
					'kendo.drawing.min': {
						'deps': [
							'kendo.color.min'
						]
					},
					'kendo.pdf.min': {
						'deps': [
							'kendo.drawing.min'
						]
					},
					'kendo.angular.min': {
						'deps': [
							'angular',
							'kendo.core.min',
							'kendo.data.min',
							'kendo.grid.min',
							'kendo.listview.min',
							'kendo.autocomplete.min',
							'kendo.multiselect.min'
						]
					}
				}
			};
		}
	);


