
	define([
		'underscore',
		'modules/main',
		'services/datasource',
		'directive/main/main',
		'directive/mainmenu/mainmenu',
		'view/home/home',
		'view/sites/sites',
		'less!styles/less/main'
	], function(
		_,
		Module,
		DataSourceService,
		Main,
		MainMenu,
		Home,
		Sites
	) {

		'use strict';

		function defineRoute(provider, id, url, config) {

			provider.state(id, _.extend({}, config, { url: url }));
		};

		Module.config([
			'$stateProvider',
			'$urlRouterProvider',
			'$locationProvider',
		function(
			$stateProvider,
			$urlRouterProvider,
			$locationProvider
		) {

			var route = defineRoute.bind(null, $stateProvider);
			$urlRouterProvider.otherwise('/');
			route('home', '/', Home);
			route('sites', '/sites', Sites);
		}]);

		Module.factory('DataSourceService', DataSourceService);

		angular.bootstrap(document, [ Module.name ]);
	});
