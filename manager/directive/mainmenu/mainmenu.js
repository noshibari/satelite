
	define([
		'modules/main',
		'text!./mainmenu.html',
		'less!./mainmenu.less'
	], function(
		Module,
		Template,
		Less
	) {

	'use strict';

	Module.directive('mainMenu', [function() {

		var controller = ['$scope', '$state', function ($scope, $state) {

			$scope.Menu = {
				home: { Text: 'Home' },
				sites: { Text: 'Sites' }
			};

			$scope.selectOption = function($event, Option) {

				$state.go(
					Option,
					{
						location: true,
						notify: true
					}
				);
			}

			$scope.isMyState = function(Option) {

				return $state.current.name == Option;
			}

		}];

		return {
		  restrict: 'E',
		  controller: controller,
		  template: Template,
		  replace: true
		};
	}]);
});
