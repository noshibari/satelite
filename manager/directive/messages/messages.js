
	define([
		'modules/main',
		'text!./messages.html',
		'less!./messages.less'
	], function(
		Module,
		Template,
		Less
	) {

	'use strict';

	var TimeOut = 3000;
	var Timer;

	Module.directive('messages', [function() {

		var controller = [
			'$scope',
			'$timeout',
		function (
			$scope,
			$timeout
		) {

			$scope.Message = '';
			$scope.Type = '';

			$scope.$on('Message', function($event, Data) {

				$scope.$apply(function() {

					$scope.Message = Data.Message;
					$scope.Type = Data.Type || 'Info';
				});

				$timeout.cancel(Timer);

				Timer = $timeout(function() {

					$scope.Message = '';
				}, TimeOut)
			});
		}];

		return {
		  restrict: 'E',
		  controller: controller,
		  template: Template,
		  replace: true
		};
	}]);
});
