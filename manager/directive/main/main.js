	define([
		'modules/main',
		'text!./main.html',
		'less!./main'
	], function(
		Module,
		Template,
		Less
	) {

	'use strict';

	Module.directive('main', [function() {

		var controller = ['$q', function ($q) {

		}];

		return {
		  restrict: 'E',
		  controller: controller,
		  template: Template,
		  replace: true
		};
	}]);
});
